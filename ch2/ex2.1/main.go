package main

import (
	"fmt"

	"gitlab.com/sascha.lange/gopl.io-excersises/ch2/ex2.1/tempconv"
)

func main() {
	fmt.Println(tempconv.CToF(tempconv.AbsoluteZeroC))
	fmt.Println(tempconv.CToK(tempconv.BoilingC))
}
